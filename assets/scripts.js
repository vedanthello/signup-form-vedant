let form = document.getElementsByTagName("form")[0];
let firstName = document.getElementById("first-name");
let lastName = document.getElementById("last-name");
let email = document.getElementById("email");
let password = document.getElementById("password");

form.addEventListener("submit", event => {
  event.preventDefault();
  checkAllInputs();
});

function checkAllInputs() {
  if (firstName.value === "") {
    setCommonErrors(firstName, "First Name cannot be empty");
  } else {
    unsetCommonErrors(firstName);
  }

  if (lastName.value === "") {
    setCommonErrors(lastName, "Last Name cannot be empty");
  } else {
    unsetCommonErrors(lastName);
  }

  if (email.value === "") {
    setCommonErrors(email, "Email Address cannot be empty");
  } else if(/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i.test(email.value) === false) {
    email.style.color = "#EE7B82";
    setCommonErrors(email, "Looks like this is not an email");
  } else {
    email.style.color = "#3D3A43";
    unsetCommonErrors(email);
  }

  if (password.value === "") {
    setCommonErrors(password, "Password cannot be empty");
  } else {
    unsetCommonErrors(password);
  }
}

function setCommonErrors(input, message) {
  let formControl = input.parentElement;
  let small = formControl.querySelector("small");
  small.innerText = message;
  formControl.className = "form-control error";
}

function unsetCommonErrors(input) {
  let formControl = input.parentElement;
  let small = formControl.querySelector("small");
  small.innerText = "";
  formControl.className = "form-control";
}