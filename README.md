# Task
 Build out the introductory component mentioned in the GitLab's repo whose link is: https://gitlab.com/mountblue/cohort-12-javascript-2/html-css-signup-form, for both mobile and desktop.

# Notes:
1. For mobile version, the screen width is 375px, whereas, for desktop version, the screen width is 1440px.
2. Get the introductory component look as close as possible.
3. Users should be able to:
   * See hover states for all interactive elements on the page.
   * Receive an error message when the form is submitted if:
     * Any input field is empty. The message for this error should say "[Field Name] cannot be empty".
     * The email address is not formatted correctly (i.e. a correct email address should have this    structure:name@host.tld). The message for this error should say "Looks like this is not an email".   

# Issues:
1. In desktop version, when the form is submitted and if any of the fields are invalid, then the text on the left side of the page shifts down.     